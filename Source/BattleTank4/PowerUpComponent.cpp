// (C) 2018 Brian K. Trotter

#include "PowerUpComponent.h"
#include "Tank.h"
#include "TankTurret.h"
#include "TankBarrel.h"
#include "TankPowerup.h"
#include "TankPlayerController.h"
#include "Engine/World.h"


void UPowerUpComponent::TriggerPowerupSound()
{
	auto OwningPowerup = Cast<ATankPowerup>(GetOwner());
	if (OwningPowerup)
	{
		OwningPowerup->PlayPowerUpSound();
	}

}

void UPowerUpComponent::BeginPlay()
{
	OnComponentBeginOverlap.AddDynamic(this, &UPowerUpComponent::OnBeginOverLap);
}

void UPowerUpComponent::OnBeginOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	auto OtherTank= Cast<ATank>(OtherActor);
	if (OtherTank)
	{
		auto Controller = Cast<ATankPlayerController>(OtherTank->Controller);
		if (Controller)
		{
			if(PowerUpType==EPowerUpType::TurretSpeed)
			{
				AdjustTurretSpeed(OtherTank);
			} else if (PowerUpType == EPowerUpType::BarrelSpeed)
			{
				AdjustBarrelSpeed(OtherTank);
			}		
		}
	}
}

void UPowerUpComponent::AdjustBarrelSpeed(ATank * OtherTank)
{
	auto Barrel = OtherTank->FindComponentByClass<UTankBarrel>();
	if (Barrel)
	{
		Barrel->IncreaseDegreesPerSecond(AmountToAdjust);
		TriggerPowerupSound();
		GetOwner()->Destroy();
	}
}

void UPowerUpComponent::AdjustTurretSpeed(ATank * OtherTank)
{
	auto Turret = OtherTank->FindComponentByClass<UTankTurret>();
	if (Turret)
	{
		Turret->IncreaseDegreesPerSecond(AmountToAdjust);
		TriggerPowerupSound();
		GetOwner()->Destroy();
	}
}
