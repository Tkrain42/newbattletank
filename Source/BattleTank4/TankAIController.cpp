// (C) 2018 Brian K. Trotter

#include "TankAIController.h"
#include "TankPlayerController.h"
#include "Tank.h"
#include "TankAimingComponent.h"
#include "PatrolRoute.h"
#include "Engine/World.h"


ATank* ATankAIController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}

ATank * ATankAIController::GetPlayerTank() const
{

	return Cast<ATankPlayerController>(GetWorld()->GetFirstPlayerController())->GetControlledTank();
}

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
	ControlledTank = GetControlledTank();
	TankAimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	
}

void ATankAIController::Tick(float DeltaTime)
{
	if (!ControlledTank->TankIsAlive())
	{
		return;
	}
	if (PatrolStatus==EPatrolStatus::Idle)
	{
		return;
	}
	if (PatrolStatus==EPatrolStatus::Patrol)
	{
		
		FollowPatrolRoute();
		
		return;
	}
	
	if (PatrolStatus==EPatrolStatus::Pursuit)
	{
		PursuePlayer();
	}
}

void ATankAIController::PursuePlayer()
{
	AimAtPlayer();


	if (ensure(TankAimingComponent))
	{

		if (TankAimingComponent->GetFiringState() == EFiringStatus::Ready)
		{
			GetControlledTank()->GetTankAimingComponent()->Fire();
		}
		if (ensure(GetPlayerTank()))
		{
			MoveToActor(GetPlayerTank(), AcceptanceRadius, true, true, false);
		}
	}
}

void ATankAIController::FollowPatrolRoute()
{
	
	if (!CurrentWaypoint)
	{
		CurrentWaypoint = GetNextWaypoint();
		if (!CurrentWaypoint)
		{
			UE_LOG(LogTemp, Warning, TEXT("%s could not find a waypoint.  Going idle"), *GetControlledTank()->GetName())
				PatrolStatus = EPatrolStatus::Idle;
			return;
		}
	}
	//UE_LOG(LogTemp, Warning, TEXT("%s is waypoint."), *CurrentWaypoint->GetName())
	MoveToActor(CurrentWaypoint, 1, true, true, false);
	if (FVector::Dist(CurrentWaypoint->GetActorLocation(), ControlledTank->GetActorLocation()) <= PatrolAcceptanceRadius)
	{
		CurrentWaypoint = nullptr;
	}
	
}

void ATankAIController::AimAtPlayer() 
{
	if (!ensure(GetControlledTank())) {
		return;
	}
	if (!ensure(GetPlayerTank())) {
		return;
	}
	if (!ensure(GetControlledTank()->GetTankAimingComponent()))
	{
		return;
	}
	GetControlledTank()->GetTankAimingComponent()->AimAt(GetPlayerTank()->GetActorLocation());
}

AActor* ATankAIController::GetNextWaypoint()
{
	AActor* result = nullptr;
	UE_LOG(LogTemp, Warning, TEXT("%s is choosing a waypoint."), *GetControlledTank()->GetName())
	auto PatrolRoute = GetPawn()->FindComponentByClass<UPatrolRoute>();
	if (PatrolRoute)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s found a PatrolRoute"), *GetControlledTank()->GetName())
		result = PatrolRoute->GetWaypointAt(NextWaypoint);
		NextWaypoint++;
		NextWaypoint = NextWaypoint % PatrolRoute->GetWaypointCount();
	}
	if (result)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s found a waypoint"), *GetControlledTank()->GetName())
	}
	return result;
}
