// (C) 2018 Brian K. Trotter

#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Tank.h"


// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Movement=CreateDefaultSubobject<UProjectileMovementComponent>(FName("ProjectileMovement"));
	Movement->bAutoActivate = false;
	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("CollisionMesh"));
	SetRootComponent(CollisionMesh);
	CollisionMesh->SetNotifyRigidBodyCollision(true);
	CollisionMesh->SetVisibility(false);

	LaunchBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("LaunchBlast"));
	LaunchBlast->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	LaunchBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	LaunchBlast->Activate();
	ExplosionForce = FindComponentByClass<URadialForceComponent>();
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::LaunchProjectile(float Speed)
{
	Movement->SetVelocityInLocalSpace(FVector::ForwardVector*Speed);
	Movement->Activate();
}

void AProjectile::OnCollision(AActor* ObjectHit)
{
	if (ObjectHit == GetOwner())
	{
		return;
	}
	if (Cast<ATank>(ObjectHit))
	{
		UGameplayStatics::ApplyRadialDamageWithFalloff(
			this,
			ProjectileDamage,
			ProjectileDamage / 10,
			GetActorLocation(),
			ExplosionForce->Radius / 10,
			ExplosionForce->Radius,
			Falloff,
			UDamageType::StaticClass(),
			TArray<AActor*>(),
			GetOwner()
		);
	} else
	{
		UGameplayStatics::ApplyRadialDamageWithFalloff(
			this,
			ProjectileDamage/2,
			ProjectileDamage / 10,
			GetActorLocation(),
			ExplosionForce->Radius / 10,
			ExplosionForce->Radius,
			Falloff,
			UDamageType::StaticClass(),
			TArray<AActor*>(),
			GetOwner()
		);
	}
	if (ensure(ExplosionForce))
	{
		ExplosionForce->FireImpulse();
	}
	
	this->Destroy();
}

