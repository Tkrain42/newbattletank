// (C) 2018 Brian K. Trotter

#include "TankBarrel.h"
#include "Engine/World.h"

void UTankBarrel::Elevate(float RelativeSpeed) {
	float ElevationChange = FMath::Clamp(RelativeSpeed,-1.0f,1.0f) * MaxDegreesPerSecond* GetWorld()->DeltaTimeSeconds;
	lastElevationChange = RelativeSpeed;
	float NewElevation = FMath::Clamp(RelativeRotation.Pitch + ElevationChange, MinElevationDegrees, MaxElevationDegrees);
	SetRelativeRotation(FRotator(NewElevation,0,0));

}

bool UTankBarrel::IsProperlyElevated()
{
	return FMath::Abs(lastElevationChange) < 1;
}

void UTankBarrel::IncreaseDegreesPerSecond(float DegreesToAdd)
{
	MaxDegreesPerSecond += DegreesToAdd;
	MaxDegreesPerSecond = FMath::Clamp(MaxDegreesPerSecond, 5.0f, 355.0f);
}
