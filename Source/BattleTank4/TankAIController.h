// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "TankAIController.generated.h"

class ATank;
class UTankAimingComponent;

UENUM()
enum class EPatrolStatus :uint8
{
	Patrol,       //AI will nagivate patrol points
	Pursuit,	  //AI will directly pursue enemy
	Investigate,   //AI will invistigate last known positions of enemy
	Idle          //If there is no patrol route, then it should go to idle instead of Patrol.
};

/**
 * 
 */
UCLASS()
class BATTLETANK4_API ATankAIController : public AAIController
{
	GENERATED_BODY()
	
	public:
	UFUNCTION(BlueprintPure)
		ATank* GetControlledTank() const;

	UFUNCTION(BlueprintPure)
		ATank* GetPlayerTank() const;

	UPROPERTY(EditDefaultsOnly)
		float AcceptanceRadius=5000;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
		bool UsingBehaviorTree = false;

	UPROPERTY(BlueprintReadWrite)
		AActor* CurrentWaypoint = nullptr;
	void BeginPlay() override;

	
	virtual void Tick(float DeltaTime) override;

	void PursuePlayer();

	void FollowPatrolRoute();

	void AimAtPlayer();
	
protected:
	ATank* ControlledTank;
	UTankAimingComponent* TankAimingComponent;

	UPROPERTY(BlueprintReadWrite, Category = "Patrol", meta = (ToolTip = "The current patrol status of the tank."))
	EPatrolStatus PatrolStatus = EPatrolStatus::Patrol;

	UFUNCTION(BlueprintCallable, Category = "Patrol", meta = (ToolTip = "Finds Next waypoint in the controlled tank's Patrol Route"))
	AActor* GetNextWaypoint();

	UPROPERTY(BlueprintReadWrite, Category = "Patrol", meta = (ToolTip = "Next patrol waypoint."))
	int32 NextWaypoint = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Patrol", meta = (Tooltip = "Acceptance radius for patrol points"))
	float PatrolAcceptanceRadius = 500;

private:

	
};
