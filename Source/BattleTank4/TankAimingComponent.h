// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TankAimingComponent.generated.h"

class UTankBarrel;
class UTankTurret;
class AProjectile;

UENUM()
enum class EFiringStatus :uint8
{
	Ready,
	Aiming,
	Reloading
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK4_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void SetComponents(UTankBarrel* BarrelToSet, UTankTurret* TurretToSet);

	


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void AimAt(FVector TargetLocation);

	void SetBarrel(UTankBarrel* BarrelToSet);
	void SetTurret(UTankTurret* TurretToSet);


	UFUNCTION(BlueprintPure, Category="Firing")
		EFiringStatus GetFiringState() const;

	UFUNCTION(BlueprintPure, Category="Firing")
		int32 GetAmmoCount() const;

	UFUNCTION(BlueprintCallable, Category="Firing")
	void Fire();

	void FireAtSocket(const FName &SocketName);

	

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="Setup")
	TSubclassOf<AProjectile> ProjectileBlueprint;

protected:
	UFUNCTION(BlueprintCallable, Category="Setup")
	void SetProjectileBlueprint(TSubclassOf<AProjectile> ProjectileBluePrintToSet);
	
	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetLaunchSpeed(float LaunchSpeedToSet);

	UFUNCTION(BlueprintCallable, Category="Setup")
		void SetReloadSpeed(float ReloadSpeedToSet);

	UFUNCTION(BlueprintCallable, Category="Setup")
		void SetAmmo(int32 NewAmmo);

	//Max amount of ammo the tank should hold.  Bear in mind that the UI is currently configured to handle 3 ammo
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Firing")
	int32 MaxAmmo = 3;

	//Launch speed of projectile, used for shot calculation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Firing")
	float LaunchSpeed = 100000.0f;

	//Time to reload a shot into the barrel.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Firing")
	float ReloadSpeed = 5.0f;

	//Time to regen an ammo.  Ammo will not regen if ammo is full.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Firing")
	float NewAmmoSpeed = 10.0f;
	
	UPROPERTY(BlueprintReadWrite)
	UTankBarrel *Barrel=nullptr;

	UPROPERTY(BlueprintReadWrite)
	UTankTurret *Turret = nullptr;

	//Set this for complex barrels that can fire multiple rounds at once.
	//Firing points should be named "Projectile, Projectile2, Projectile3, etc"
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setup")
		int32 BarrelCount = 1;

private:
	
	
	
	float LastShot = 0;
	int32 Ammo = 3;
	

	
	float LastAmmoBoost = 0;
	

	void ElevateBarrel(FVector AimDirection);
	void AzimuthTurret(FVector AimDirection);

	EFiringStatus FiringState = EFiringStatus::Ready;
	float LastDeltaRotation = 0;
	float LastDeltaAzimuth = 0;
};
