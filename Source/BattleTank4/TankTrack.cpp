// (C) 2018 Brian K. Trotter

#include "TankTrack.h"
#include "Engine/World.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = true;
	//OnComponentHit.AddDynamic(this, &UTankTrack::OnHit);
}

void UTankTrack::BeginPlay()
{
	Super::BeginPlay();
	OnComponentHit.AddDynamic(this, &UTankTrack::OnHit);
}

void UTankTrack::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	
}

void UTankTrack::ApplySidewaysForce(float DeltaTime)
{
	auto SlippageSpeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
	auto CorrectionAccelleration = -SlippageSpeed / DeltaTime * GetRightVector();
	auto TankRoot = Cast<UStaticMeshComponent>(GetOwner()->GetRootComponent());
	auto CorrectionForce = (TankRoot->GetMass() * CorrectionAccelleration) / 2;
	TankRoot->AddForce(CorrectionForce);
}

void UTankTrack::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	ApplySidewaysForce(GetWorld()->DeltaTimeSeconds);
	DriveTrack();
}

void UTankTrack::SetThrottle(float Throttle)
{
	CurrentThrottle += Throttle;
	CurrentThrottle = FMath::Clamp(CurrentThrottle, -1.0f, 1.0f);
}

void UTankTrack::DriveTrack()
{
	float ClampedThrottle = FMath::Clamp(CurrentThrottle, -1.0f, 1.0f);
	auto ForceApplied = GetForwardVector()*ClampedThrottle*MaxAccelerationPerTrack;
	auto ForceLocation = GetComponentLocation();
	AddForceAtLocation(ForceApplied, ForceLocation);
	CurrentThrottle = 0;
}


