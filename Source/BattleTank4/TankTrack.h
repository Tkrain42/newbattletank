// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 *  Stores default limits and moves tracks.
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK4_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
		void SetThrottle(float Throttle);

private:
	//Amount of acceleration for each track in Newtons
	UPROPERTY(EditDefaultsOnly)
		float MaxAccelerationPerTrack = 400000.0f;

	UTankTrack();

	float CurrentThrottle = 0;

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ApplySidewaysForce(float DeltaTime);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	void DriveTrack();
};
