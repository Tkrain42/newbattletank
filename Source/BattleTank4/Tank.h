// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tank.generated.h"


class UTankAimingComponent;
class UParticleSystemComponent;

class AProjectile;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTankDelegate);

UCLASS()
class BATTLETANK4_API ATank : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATank();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float LastShot = 0.0f;


	UPROPERTY(BlueprintReadOnly,EditAnywhere)
		int32 MaxHealth=100;
	UPROPERTY(BlueprintReadwrite)
		int32 CurrentHealth;

	UFUNCTION(BlueprintCallable)
		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Scoring")
		int32 CurrentScore = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death")
		UParticleSystemComponent* ParticleFlame;

public:	

	
	FTankDelegate OnDeath;


	UFUNCTION(BlueprintCallable)
		void ScaleTankHealth(float Amount);

	UFUNCTION(BlueprintPure)
		bool TankIsAlive();

	UFUNCTION(BlueprintPure)
		float GetHealthAsPercentage() const;

	UFUNCTION(BlueprintCallable)
		int32 AddToScore(int32 PointsToAdd);

	UFUNCTION(Blueprintpure)
		int32 GetScore() const;

	void DamageTank(float Intensity);

	UFUNCTION(BlueprintPure)
	UTankAimingComponent* GetTankAimingComponent() const;
protected:

	
};
