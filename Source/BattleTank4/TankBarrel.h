// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"

/**
 * 
 */
UCLASS(meta=(BlueprintSpawnableComponent))
class BATTLETANK4_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()
	
	
public:
	void Elevate(float RelativeSpeed);
	
	bool IsProperlyElevated();
	
	UFUNCTION(BlueprintCallable)
		void IncreaseDegreesPerSecond(float DegreesToAdd);
	
private:
	UPROPERTY(EditAnywhere, Category = "Setup")
		float MaxDegreesPerSecond = 20;
	UPROPERTY(EditAnywhere, Category = "Setup")
		float MaxElevationDegrees = 40;
	UPROPERTY(EditAnywhere, Category = "Setup")
		float MinElevationDegrees = 0;

	float lastElevationChange;
	
};
