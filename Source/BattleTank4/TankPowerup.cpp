// (C) 2018 Brian K. Trotter

#include "TankPowerup.h"


// Sets default values
ATankPowerup::ATankPowerup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATankPowerup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATankPowerup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	auto CurrentRotation=GetActorRotation();
	CurrentRotation.Yaw += Speed * DeltaTime;
	SetActorRotation(CurrentRotation);
}

