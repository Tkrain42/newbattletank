// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TankPowerup.generated.h"

UCLASS()
class BATTLETANK4_API ATankPowerup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATankPowerup();

	UFUNCTION(BlueprintImplementableEvent)
		void PlayPowerUpSound();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Speed = 10;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
