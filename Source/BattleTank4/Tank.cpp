// (C) 2018 Brian K. Trotter

#include "Tank.h"
#include "TankAimingComponent.h"
#include "Projectile.h"
#include "Particles/ParticleSystemComponent.h"



// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
	ParticleFlame = FindComponentByClass<UParticleSystemComponent>();
}


float ATank::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	if (!TankIsAlive())
	{
		return 0;
	}
	int32 IntDamage = FMath::FloorToInt(DamageAmount);
	int32 ActualDamage = FMath::Min(IntDamage, CurrentHealth);
	CurrentHealth -= ActualDamage;
	auto TankThatHitMe = Cast<ATank>(DamageCauser);
	if (TankThatHitMe)
	{
		TankThatHitMe->AddToScore(ActualDamage);
	}
		if (!TankIsAlive())
		{
			if (TankThatHitMe)
			{
				TankThatHitMe->AddToScore(MaxHealth);
			}
			if (ParticleFlame)
			{
				ParticleFlame->Activate();
			}
			
			OnDeath.Broadcast();
		}
	return ActualDamage;
}

void ATank::ScaleTankHealth(float Amount)
{
	Amount = FMath::Clamp(Amount, .1f, 30000.0f); //Prevent division by zed errors
	float NewMaxHealth = MaxHealth * Amount;
	MaxHealth =FMath::FloorToInt(NewMaxHealth);
	MaxHealth = FMath::Max(MaxHealth, 10); 
	CurrentHealth = MaxHealth;
}

bool ATank::TankIsAlive()
{
	return (CurrentHealth>0.0);
}

float ATank::GetHealthAsPercentage() const
{
	if (MaxHealth == 0)
	{
		return 0;
	}
	float CurrentAsFloat = CurrentHealth;
	float MaxAsFloat = MaxHealth;
	return CurrentAsFloat / MaxAsFloat;
}

int32 ATank::AddToScore(int32 PointsToAdd)
{
	CurrentScore += PointsToAdd;
	UE_LOG(LogTemp, Warning, TEXT("Adding %i points to score.  Score is now %i"), PointsToAdd, CurrentScore)
	return CurrentScore;
}

int32 ATank::GetScore() const
{
	return CurrentScore;
}

void ATank::DamageTank(float Intensity)
{
	UE_LOG(LogTemp, Warning, TEXT("%s was damaged with %f intensity!"), *GetName(), Intensity)
}

UTankAimingComponent * ATank::GetTankAimingComponent() const
{
	return FindComponentByClass<UTankAimingComponent>();
}

