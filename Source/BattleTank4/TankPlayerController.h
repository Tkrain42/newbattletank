// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "TankPlayerController.generated.h"


class ATank;
/**
 * 
 */
UCLASS()
class BATTLETANK4_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
	ATank* GetControlledTank() const;


	UFUNCTION(BlueprintImplementableEvent)
		void AimingComponentIsAssigned(UTankAimingComponent* TankAimingComponent);



	void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float CrossHairX = .5f;

	UPROPERTY(EditAnywhere)
		float CrossHairY = .33f;

	UPROPERTY(EditAnywhere)
		float DistanceToRayTrace = 1000000.0f;

	void AimTowardsCrosshairs(float DeltaTime);
	
protected:
	UTankAimingComponent* TankAimingComponent;
	ATank* ControlledTank;

	UPROPERTY(EditDefaultsOnly)
		float ScaleAmount = 1.0f;

private:
	bool GetLookVectorHitLocation(FVector LookDirection, FVector & OutHitLocation) const;
	FVector2D GetScreenLocation() const;
	bool GetSightRayHitLocation(FVector &OutHitLocation) const;

	bool GetLookDirection(FVector & LookDirection) const;
	
	virtual void SetPawn(APawn* InPawn) override;
	UFUNCTION()
		void OnTankDeath();

};
