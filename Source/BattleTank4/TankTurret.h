// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTurret.generated.h"

/**
 * 
 */
UCLASS(meta=(BlueprintSpawnableComponent))
class BATTLETANK4_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()
	

public:
	void Azimuth(float RelativeSpeed);
	bool IsProperlyRotated();

	UFUNCTION(BlueprintCallable)
	void IncreaseDegreesPerSecond(float DegreesToAdd);

	UFUNCTION(BlueprintCallable)
		float MaxMovementThisFrame();


private:
	UPROPERTY(EditAnywhere)
		float MaxDegreesPerSecond = 20.0f;

	float LastAzimuthChange = 0;
	
	
};
