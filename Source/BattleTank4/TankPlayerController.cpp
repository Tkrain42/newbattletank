// (C) 2018 Brian K. Trotter

#include "TankPlayerController.h"
#include "Tank.h"
#include "TankAimingComponent.h"
#include "Engine/World.h"


ATank* ATankPlayerController::GetControlledTank() const
{
	
	return Cast<ATank>(GetPawn());
}



void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	ControlledTank = GetControlledTank();
	if (!ControlledTank) { return; }
	TankAimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	AimingComponentIsAssigned(TankAimingComponent);
	ControlledTank->ScaleTankHealth(ScaleAmount);
}

void ATankPlayerController::Tick(float DeltaTime)
{
	if (!ControlledTank)
	{
		return;
	}
	if (!ControlledTank->TankIsAlive())
	{
		return;
	}
	AimTowardsCrosshairs(DeltaTime);
}

void ATankPlayerController::AimTowardsCrosshairs(float DeltaTime)
{
	if (!(ControlledTank)) {
		return;
	}
	if (!(TankAimingComponent))
	{
		return;
	}
	FVector HitLocation;
	if (GetSightRayHitLocation(HitLocation)) {
		TankAimingComponent->AimAt(HitLocation);
	}

}




bool ATankPlayerController::GetSightRayHitLocation(FVector &OutHitLocation) const
{
	FVector LookDirection;
	if (GetLookDirection(LookDirection)) 
	{
		return GetLookVectorHitLocation(LookDirection, OutHitLocation);
	}
	else {
		return false;
	}
}


bool ATankPlayerController::GetLookVectorHitLocation(FVector LookDirection, FVector &OutHitLocation) const
{
	FHitResult HitResult;
	FVector StartLocation = PlayerCameraManager->GetCameraLocation();
	FVector EndLocation = StartLocation + LookDirection * DistanceToRayTrace;
		if (GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Camera)) 
			{
				OutHitLocation = HitResult.Location;
				return true;
			}
		else {
			return false;
		}
	
}

FVector2D ATankPlayerController::GetScreenLocation() const
{
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	return FVector2D(ViewportSizeX*CrossHairX, ViewportSizeY*CrossHairY);
}

bool ATankPlayerController::GetLookDirection(FVector &LookDirection) const 
{
	
	FVector CameraLocation;
	FVector2D ScreenLocation = GetScreenLocation();
	return DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CameraLocation, LookDirection);
}

void ATankPlayerController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (PossessedTank)
		{
			PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnTankDeath);
		}
	}
}

void ATankPlayerController::OnTankDeath()
{
	UE_LOG(LogTemp, Warning, TEXT("Tank %s is dead!"), *GetName())
}
