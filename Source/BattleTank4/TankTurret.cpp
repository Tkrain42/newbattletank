// (C) 2018 Brian K. Trotter

#include "TankTurret.h"
#include "Engine/World.h"

void UTankTurret::Azimuth(float RelativeSpeed)
{
	float AzimuthChange = FMath::Clamp(RelativeSpeed,-1.0f,1.0f) * MaxDegreesPerSecond* GetWorld()->DeltaTimeSeconds;
	if (FMath::Abs(AzimuthChange) > FMath::Abs(RelativeSpeed))
	{
		AzimuthChange = RelativeSpeed; //Avoid the jittery barrel
	}
	LastAzimuthChange = AzimuthChange;
	float NewAzimuth = RelativeRotation.Yaw + AzimuthChange;
	SetRelativeRotation(FRotator(0, NewAzimuth, 0));
}

bool UTankTurret::IsProperlyRotated()
{
	return FMath::Abs(LastAzimuthChange)<1.0f;
}

void UTankTurret::IncreaseDegreesPerSecond(float DegreesToAdd)
{
	MaxDegreesPerSecond += DegreesToAdd;
	MaxDegreesPerSecond = FMath::Clamp(MaxDegreesPerSecond, 5.0f, 355.0f);
}

float UTankTurret::MaxMovementThisFrame()
{
	return MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
}
