// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "TankNavMovementComponent.generated.h"

class UTankTrack;
/**
 * 
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BATTLETANK4_API UTankNavMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()
	
public:
		UFUNCTION(BlueprintCallable)
		void SetTracks(UTankTrack* LeftTrackToSet, UTankTrack* RightTrackToSet);	

		UFUNCTION(BlueprintCallable)
			void IntendMoveForward(float Throw);

		UFUNCTION(BlueprintCallable)
			void IntendTurn(float Throw);

		virtual void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;
	
protected:
	UTankTrack* LeftTrack=nullptr;
	UTankTrack* RightTrack=nullptr;
};
