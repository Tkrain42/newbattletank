// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class UProjectileMovementComponent;
class UParticleSystemComponent;
class URadialForceComponent;


UCLASS()
class BATTLETANK4_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UProjectileMovementComponent* Movement;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* CollisionMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UParticleSystemComponent* LaunchBlast;
	UPROPERTY(BlueprintReadWrite)
		AActor* Explosion;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		URadialForceComponent* ExplosionForce;
	UPROPERTY(EditDefaultsOnly)
		float ProjectileDamage = 10;
	UPROPERTY(EditAnywhere)
		float Falloff = .1f;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void LaunchProjectile(float Speed);
	
	UFUNCTION(BlueprintCallable)
		void OnCollision(AActor* ObjectHit);
};
