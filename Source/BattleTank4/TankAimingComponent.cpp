// (C) 2018 Brian K. Trotter

#include "TankAimingComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
	Ammo = MaxAmmo;
	
	
}

void UTankAimingComponent::SetComponents(UTankBarrel * BarrelToSet, UTankTurret * TurretToSet)
{
	Barrel = BarrelToSet;
	Turret = TurretToSet;

}

void UTankAimingComponent::SetBarrel(UTankBarrel * BarrelToSet)
{
	if (ensure(BarrelToSet)) {
		Barrel = BarrelToSet;
	 }
}

void UTankAimingComponent::SetTurret(UTankTurret * TurretToSet)
{
	if (ensure(TurretToSet)) {
		Turret = TurretToSet;
	}
}

EFiringStatus UTankAimingComponent::GetFiringState() const
{
	return FiringState;
}

int32 UTankAimingComponent::GetAmmoCount() const
{
	return Ammo;
}

void UTankAimingComponent::Fire()
{
	if (!ensure(Barrel)) { 
		UE_LOG(LogTemp, Warning, TEXT("No Barrel!"))
		return; }
	if (!ensure(ProjectileBlueprint))
	{
		UE_LOG(LogTemp, Warning, TEXT("No Projectile set!"))
			return;
	}
	if (LastShot < ReloadSpeed)
	{
		return;
	}
	if (Ammo < 1)
	{
		return;
	}
	FName SocketName = "Projectile";
	FireAtSocket(SocketName);
	if (BarrelCount > 1)
	{
		SocketName = "Projectile2";
		FireAtSocket(SocketName);
		if (BarrelCount > 2)
		{
			SocketName = "Projectile3";
			FireAtSocket(SocketName);
		}
	}
	
	LastShot =0;
	Ammo -= 1;
}

void UTankAimingComponent::FireAtSocket(const FName &SocketName)
{
	AProjectile* NewProjectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint, Barrel->GetSocketLocation(SocketName), Barrel->GetForwardVector().ToOrientationRotator());
	NewProjectile->SetOwner(GetOwner());
	NewProjectile->LaunchProjectile(LaunchSpeed);
}





void UTankAimingComponent::SetProjectileBlueprint(TSubclassOf<AProjectile> ProjectileBluePrintToSet)
{
	ProjectileBlueprint = ProjectileBluePrintToSet;
}

void UTankAimingComponent::SetLaunchSpeed(float LaunchSpeedToSet)
{
	LaunchSpeed = LaunchSpeedToSet;
}

void UTankAimingComponent::SetReloadSpeed(float ReloadSpeedToSet)
{
	ReloadSpeed = FMath::Clamp(ReloadSpeedToSet, .25f, 120.0f);
}

void UTankAimingComponent::SetAmmo(int32 NewAmmo)
{
	Ammo = NewAmmo;
}

void UTankAimingComponent::ElevateBarrel(FVector AimDirection)
{
	if (!ensure(Barrel)) { return; }
	auto DesiredRotation = AimDirection.ToOrientationRotator().Pitch;
	auto BarrelRotation = Barrel->GetForwardVector().ToOrientationRotator().Pitch;
	//UE_LOG(LogTemp, Warning, TEXT("ElevateBarrel: %s - AimDirection=%s -> %s"), *GetOwner()->GetName(), *AimDirection.ToString(), *DesiredRotation.ToString())
	float DeltaRotation = DesiredRotation - BarrelRotation;
	LastDeltaRotation = DeltaRotation;
	Barrel->Elevate(FMath::Sign(DeltaRotation));
	
}

void UTankAimingComponent::AzimuthTurret(FVector AimDirection)
{
	if (!ensure(Turret)) { return; }
	float DesiredYaw = AimDirection.ToOrientationRotator().Yaw;
	float TurretYaw = Turret->GetForwardVector().ToOrientationRotator().Yaw;
	float DeltaRotation = DesiredYaw - TurretYaw;
	if (FMath::Abs(DeltaRotation) > 180)
	{
		DeltaRotation = -DeltaRotation;
	}
	if (FMath::Abs(DeltaRotation) < Turret->MaxMovementThisFrame())
	{
		DeltaRotation /= Turret->MaxMovementThisFrame();
	}
	LastDeltaAzimuth = FMath::Abs(DeltaRotation);
	Turret->Azimuth(DeltaRotation);
}


// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	if (Ammo == MaxAmmo)
	{
		LastAmmoBoost = 0; //Do not regen ammo if we're full
	} else
	{
		LastAmmoBoost += DeltaTime;
		if ( LastAmmoBoost > NewAmmoSpeed)
		{
			Ammo += 1;
			LastAmmoBoost = 0;
		}
	}


	bool LockedAndLoaded = (LastDeltaRotation<1) & (LastDeltaAzimuth<1);
	LastShot += DeltaTime;
	if (LastShot < ReloadSpeed)
	{
		FiringState = EFiringStatus::Reloading;
	} else if (Ammo < 1) 
	{
		FiringState = EFiringStatus::Reloading;
	} else if (LockedAndLoaded)
	{
		FiringState = EFiringStatus::Ready;
	} else
	{
		FiringState = EFiringStatus::Aiming;
	}

	
}

void UTankAimingComponent::AimAt(FVector TargetLocation)
{
	if (!Barrel) { return; }
	if (!Turret) { return; }
	FVector TossVelocity;
	FVector StartLocation = Barrel->GetSocketLocation(FName("Projectile"));
	
	if (UGameplayStatics::SuggestProjectileVelocity(
		this,
		TossVelocity,
		StartLocation,
		TargetLocation,
		LaunchSpeed,
		false,
		0,
		0,
		ESuggestProjVelocityTraceOption::DoNotTrace,
		FCollisionResponseParams::DefaultResponseParam,
		TArray<AActor*>(),
		false
	))
	{
		FVector AimDirection = TossVelocity.GetSafeNormal();
		ElevateBarrel(AimDirection);
		AzimuthTurret(AimDirection);
		//UE_LOG(LogTemp,Warning,TEXT("%s aiming at %s"), *GetOwner()->GetName(), *AimDirection.ToString())
	}
	
}

