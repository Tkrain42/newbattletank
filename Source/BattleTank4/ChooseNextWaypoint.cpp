// (C) 2018 Brian K. Trotter

#include "ChooseNextWaypoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "PatrolRoute.h"
#include "Classes/AIController.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	auto MyController = OwnerComp.GetAIOwner();
	auto MyCharacter = MyController->GetPawn()->FindComponentByClass<UPatrolRoute>();

	
	if (MyCharacter)
	{
		
		auto Blackboard = OwnerComp.GetBlackboardComponent();
		int CurrentIndex = Blackboard->GetValueAsInt(Index.SelectedKeyName);
		AActor* NewWaypoint = MyCharacter->GetWaypointAt(CurrentIndex);
		Blackboard->SetValueAsObject(Waypoint.SelectedKeyName, NewWaypoint);
		UE_LOG(LogTemp, Warning, TEXT("%s advancing to waypoint %i of %i"), *MyCharacter->GetName(), CurrentIndex, MyCharacter->GetWaypointCount());
		CurrentIndex++;
		CurrentIndex = CurrentIndex % MyCharacter->GetWaypointCount();
		Blackboard->SetValueAsInt(Index.SelectedKeyName, CurrentIndex);
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to locate owned Third Person Character."))
	}
	return EBTNodeResult::Type();
}
