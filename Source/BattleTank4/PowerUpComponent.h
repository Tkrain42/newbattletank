// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "PowerUpComponent.generated.h"

UENUM()
enum class EPowerUpType :uint8
{
	TurretSpeed,
	BarrelSpeed,
	Accelleration,
	ReloadRate,
	RegenRate
};

class ATank;

/**
 * 
 */

UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK4_API UPowerUpComponent : public UStaticMeshComponent
{
	GENERATED_BODY()
	
	

	

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EPowerUpType PowerUpType = EPowerUpType::TurretSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AmountToAdjust = 5.0f;

	UFUNCTION(BlueprintCallable)
		void TriggerPowerupSound();

private:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnBeginOverLap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	void AdjustBarrelSpeed(ATank * OtherTank);

	void AdjustTurretSpeed(ATank * OtherTank);


};
