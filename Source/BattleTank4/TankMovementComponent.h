// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankMovementComponent.generated.h"

class UTankTrack;

/**
 * Manages tank's movement by directing Tank Tracks.
 */

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK4_API UTankMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankMovementComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UTankTrack* LeftTrack;
	UTankTrack* RightTrack;


		

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		UFUNCTION(BluePrintCallable)
		void SetTracks(UTankTrack* LeftTrackToSet, UTankTrack* RightTrackToSet);		
	
};
