// (C) 2018 Brian K. Trotter

#include "TankNavMovementComponent.h"
#include "TankTrack.h"
#include "GameFramework/Actor.h"

void UTankNavMovementComponent::SetTracks(UTankTrack * LeftTrackToSet, UTankTrack * RightTrackToSet)
{
	LeftTrack = LeftTrackToSet;
	RightTrack = RightTrackToSet;
}

void UTankNavMovementComponent::IntendMoveForward(float Throw)
{
	if (!ensure(LeftTrack) || !ensure(RightTrack))
	{
		return;
	}
	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(Throw);

}

void UTankNavMovementComponent::IntendTurn(float Throw)
{
	if (!ensure(LeftTrack) || !ensure(RightTrack))
	{
		return;
	}
	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(-Throw);
}

void UTankNavMovementComponent::RequestDirectMove(const FVector & MoveVelocity, bool bForceMaxSpeed)
{
	auto AIForwardIntention = MoveVelocity.GetSafeNormal();
	auto CurrentForward = GetOwner()->GetActorForwardVector().GetSafeNormal();
	float DotProduct = FVector::DotProduct(AIForwardIntention, CurrentForward);
	IntendMoveForward(DotProduct);
	auto CrossProduct = FVector::CrossProduct(AIForwardIntention, CurrentForward);
	IntendTurn(-CrossProduct.Z);

}
